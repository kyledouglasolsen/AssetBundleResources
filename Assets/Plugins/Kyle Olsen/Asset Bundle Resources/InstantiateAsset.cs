﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetBundleResources
{
    public class InstantiateAsset : MonoBehaviour, IResourceLoadedHandler
    {
        [SerializeField] private AutoLoadTime autoLoadTime = AutoLoadTime.Start;
        [SerializeField] private AssetLoadRequest request = new AssetLoadRequest();
        [SerializeField] private Vector3 positionOffset = Vector3.zero;
        [SerializeField] private Vector3 rotationOffset = Vector3.zero;
        private GameObject loadedGameObject;
        private GameObject instancedObject;

        public AutoLoadTime LoadTime => autoLoadTime;
        public AssetLoadRequest Request => request;

        public void SetPositionOffset(Vector3 position)
        {
            positionOffset = position;
        }

        public void SetRotationOffset(Vector3 rotation)
        {
            rotationOffset = rotation;
        }

        public void SetOffsets(Vector3 position, Vector3 rotation)
        {
            SetPositionOffset(position);
            SetRotationOffset(rotation);
        }

        public void Load()
        {
            if (loadedGameObject != null)
            {
                OnLoadComplete(loadedGameObject);
            }
            else
            {
                Request.Load(this);
            }
        }

        public void OnLoadComplete(Object loadedObject)
        {
            loadedGameObject = loadedObject as GameObject;

            if (loadedGameObject == null)
            {
                throw new Exception("Unable to load " + request.AssetName + " from bundle " + request.BundleName);
            }

            if (instancedObject != null)
            {
                Destroy(instancedObject);
                instancedObject = null;
            }

            instancedObject = Instantiate(loadedGameObject, transform.TransformPoint(positionOffset), Quaternion.Euler(transform.eulerAngles + rotationOffset), transform);
        }

        private void Awake()
        {
            if (autoLoadTime == AutoLoadTime.Awake)
            {
                Load();
            }
        }

        private void Start()
        {
            if (autoLoadTime == AutoLoadTime.Start)
            {
                Load();
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Extract Transform Data From Prefab")]
        private void ExtractTransformDataFromPrefab()
        {
            var editorPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(Request.BundleName, Request.AssetName);

            if (editorPaths.Length > 0)
            {
                var prefab = UnityEditor.AssetDatabase.LoadAssetAtPath(editorPaths[0], typeof(Object)) as GameObject;

                if (prefab != null)
                {
                    positionOffset = prefab.transform.localPosition;
                    rotationOffset = prefab.transform.localEulerAngles;
                }
            }
        }
#endif
    }
}