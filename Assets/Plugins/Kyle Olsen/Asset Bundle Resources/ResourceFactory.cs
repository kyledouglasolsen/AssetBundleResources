﻿#define IGNORE_BUNDLES

using UnityEngine;

namespace AssetBundleResources
{
    public static class ResourceFactory
    {
        private static IResourceLoader loader;

        public static IResourceLoader Loader
        {
            get
            {
                if (!LoadingEnabled)
                {
                    return null;
                }

                if (loader == null)
                {
#if UNITY_EDITOR && IGNORE_BUNDLES
                    loader = new EditorResourceLoader();
#else
                    loader = new ResourceLoader();
#endif

                    Debug.Log($"ResourceFactory created new IResourceLoader of type {loader.GetType()}");
                }

                return loader;
            }
        }

        public static bool LoadingEnabled { get; set; } = true;

        public static void SetLoadingEnabled(bool isLoadingEnabled)
        {
            LoadingEnabled = isLoadingEnabled;
        }
    }
}