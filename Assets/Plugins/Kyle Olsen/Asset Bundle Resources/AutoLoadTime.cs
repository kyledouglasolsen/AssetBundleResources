﻿namespace AssetBundleResources
{
    public enum AutoLoadTime
    {
        Never,
        Awake,
        Start
    }
}