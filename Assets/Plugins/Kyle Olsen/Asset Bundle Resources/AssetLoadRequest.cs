﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetBundleResources
{
    [Serializable]
    public class AssetLoadRequest : ILoadResource
    {
        [SerializeField] private string assetName;
        [SerializeField] private string bundleName;

        public string BundleName
        {
            get { return bundleName; }
            set { bundleName = value; }
        }

        public string AssetName
        {
            get { return assetName; }
            set { assetName = value; }
        }

        public void Load(IResourceLoadedHandler loadHandler)
        {
            ResourceFactory.Loader.LoadAsync(this, loadHandler);
        }

#if UNITY_EDITOR
        [SerializeField] [UsedImplicitly] private int bundleNameIndex;
        [SerializeField] [UsedImplicitly] private int assetNameIndex;
#endif
    }
}