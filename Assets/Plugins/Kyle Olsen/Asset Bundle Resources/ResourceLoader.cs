﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AssetBundleResources
{
    public class ResourceLoader : IResourceLoader
    {
        private static readonly PooledQueue<AssetBundleLoadRequest> LoadRequestPool = new PooledQueue<AssetBundleLoadRequest>(() => new AssetBundleLoadRequest());
        private readonly Dictionary<string, AssetBundleReference> assetBundleLookup = new Dictionary<string, AssetBundleReference>();
        private readonly List<AssetBundleReference> allAssetBundles = new List<AssetBundleReference>();
        private Timed.EveryFrameStopper everyFrameUpdate;

        public ResourceLoader()
        {
            everyFrameUpdate = Timed.EveryFrame(() =>
            {
                for (var i = 0; i < allAssetBundles.Count; ++i)
                {
                    allAssetBundles[i].Update();
                }
            });
        }

        public void Load(ILoadResource loader, IResourceLoadedHandler loadHandler)
        {
            GetBundle(loader, false).LoadAsset(loader, loadHandler);
        }

        public void LoadAsync(ILoadResource loader, IResourceLoadedHandler loadHandler)
        {
            GetBundle(loader, true).LoadAssetAsync(loader, loadHandler);
        }

        public void UnloadAll()
        {
            for (var i = 0; i < allAssetBundles.Count; ++i)
            {
                allAssetBundles[i].Unload();
            }

            allAssetBundles.Clear();
            assetBundleLookup.Clear();
        }

        public void Dispose()
        {
            everyFrameUpdate.Cancel();
            UnloadAll();
        }

        private AssetBundleReference GetBundle(ILoadResource loader, bool async)
        {
            if (!assetBundleLookup.ContainsKey(loader.BundleName))
            {
                allAssetBundles.Add(assetBundleLookup[loader.BundleName] = new AssetBundleReference(loader.BundleName, async));
            }

            return assetBundleLookup[loader.BundleName];
        }

        private class AssetBundleReference
        {
            private bool loading;
            private string bundleName;
            private List<AssetBundleLoadRequest> loadingAssets = new List<AssetBundleLoadRequest>();
            private AssetBundleCreateRequest request;

            public AssetBundleReference(string bundleName, bool async)
            {
                this.bundleName = bundleName;
                request = null;

                var path = AssetBundlePaths.FullPath + bundleName;

                if (async)
                {
                    request = AssetBundle.LoadFromFileAsync(path);

                    if (request.isDone)
                    {
                        Bundle = request.assetBundle;
                    }

                    loading = !request.isDone;
                }
                else
                {
                    Bundle = AssetBundle.LoadFromFile(path);
                    loading = false;
                }
            }

            public AssetBundle Bundle { get; private set; }

            public void LoadAsset(ILoadResource loader, IResourceLoadedHandler loadHandler)
            {
                if (Bundle == null)
                {
                    Debug.LogError($"Unable to load asset {loader.AssetName} from bundle {bundleName}");
                    return;
                }

                loadHandler.OnLoadComplete(Bundle.LoadAsset(loader.AssetName));
            }

            public void LoadAssetAsync(ILoadResource loader, IResourceLoadedHandler loadHandler)
            {
                if (!loading && Bundle == null)
                {
                    Debug.LogError($"Unable to load asset {loader.AssetName} from bundle {bundleName}");
                    return;
                }

                loadingAssets.Add(LoadRequestPool.Dequeue().Initialize(loader, loadHandler, this));
            }

            public void Unload()
            {
                if (Bundle != null)
                {
                    Bundle.Unload(false);
                }

                request = null;
            }

            public void Update()
            {
                if (loading)
                {
                    if (request == null || !request.isDone)
                    {
                        return;
                    }

                    Bundle = request.assetBundle;
                    loading = false;

                    if (Bundle == null)
                    {
                        Debug.LogError($"Failed to load asset bundle: {bundleName}");
                    }
                }

                if (Bundle == null)
                {
                    return;
                }

                for (var i = loadingAssets.Count - 1; i >= 0; --i)
                {
                    if (loadingAssets[i].IsLoadComplete())
                    {
                        loadingAssets[i].FinalizeLoad();
                        loadingAssets.RemoveAt(i);
                    }
                }
            }
        }

        private class AssetBundleLoadRequest
        {
            private AssetBundleRequest request;
            private AssetBundleReference assetBundleReference;
            private ILoadResource resourceLoader;
            private IResourceLoadedHandler resourceLoadedHandler;

            public AssetBundleLoadRequest Initialize(ILoadResource loader, IResourceLoadedHandler loadHandler, AssetBundleReference bundleReference)
            {
                resourceLoader = loader;
                resourceLoadedHandler = loadHandler;
                assetBundleReference = bundleReference;
                TryCreateRequest();

                return this;
            }

            public bool IsLoadComplete()
            {
                TryCreateRequest();

                return request != null && request.isDone;
            }

            public void FinalizeLoad()
            {
                try
                {
                    resourceLoadedHandler.OnLoadComplete(request.asset);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
                finally
                {
                    resourceLoader = null;
                    resourceLoadedHandler = null;
                    request = null;

                    LoadRequestPool.Enqueue(this);
                }
            }

            private void TryCreateRequest()
            {
                if (request != null)
                {
                    return;
                }

                if (assetBundleReference.Bundle != null)
                {
                    request = assetBundleReference.Bundle.LoadAssetAsync(resourceLoader.AssetName);
                }
            }
        }
    }
}