﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetBundleResources
{
    public class PlaySoundAsset : MonoBehaviour, IResourceLoadedHandler
    {
        [SerializeField] private AutoLoadTime _autoLoadTime = AutoLoadTime.Start;
        [SerializeField] private AudioLoadRequest _audioLoadRequest = new AudioLoadRequest();
        private AudioEvent loadedAudioEvent;

        public AutoLoadTime LoadTime => _autoLoadTime;
        public AudioLoadRequest Request => _audioLoadRequest;

        public void Load()
        {
            if (loadedAudioEvent != null)
            {
                OnLoadComplete(loadedAudioEvent);
            }
            else
            {
                _audioLoadRequest.AssetLoadRequest.Load(this);
            }
        }

        private void Awake()
        {
            if (_autoLoadTime == AutoLoadTime.Awake)
            {
                Load();
            }
        }

        private void Start()
        {
            if (_autoLoadTime == AutoLoadTime.Start)
            {
                Load();
            }
        }

        public void OnLoadComplete(Object loadedObject)
        {
            loadedAudioEvent = loadedObject as AudioEvent;

            if (loadedAudioEvent == null)
            {
                throw new Exception($"Unable to load {_audioLoadRequest.AssetLoadRequest.AssetName} from bundle {_audioLoadRequest.AssetLoadRequest.BundleName}");
            }

            AudioManager.Ins.Play(loadedAudioEvent, transform.position);
        }
    }
}