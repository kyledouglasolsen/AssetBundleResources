﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace AssetBundleResources
{
    [Serializable]
    public class AudioLoadRequest : IResourceLoadedHandler
    {
        [SerializeField] private AssetLoadRequest assetLoadRequest = new AssetLoadRequest();
        private AudioEvent audioEvent;
        private Vector3 loadPlayPosition;

        public AssetLoadRequest AssetLoadRequest => assetLoadRequest;

        public void Play(Vector3 position)
        {
            if (audioEvent != null)
            {
                AudioManager.Ins.Play(audioEvent, position);
            }
            else
            {
                loadPlayPosition = position;
                assetLoadRequest.Load(this);
            }
        }

        public void OnLoadComplete(Object loadedObject)
        {
            audioEvent = (AudioEvent)loadedObject;
            AudioManager.Ins.Play(audioEvent, loadPlayPosition);
        }
    }
}