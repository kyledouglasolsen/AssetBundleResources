﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
namespace AssetBundleResources
{
    public class EditorResourceLoader : IResourceLoader
    {
        private static readonly PooledQueue<EditorLoadRequest> EditorLoadRequestPool = new PooledQueue<EditorLoadRequest>(() => new EditorLoadRequest());

        private readonly Queue<EditorLoadRequest> editorLoadRequests = new Queue<EditorLoadRequest>(100);
        private Timed.EveryFrameStopper everyFrameUpdate;

        public EditorResourceLoader()
        {
            everyFrameUpdate = Timed.EveryFrame(() =>
            {
                while (editorLoadRequests.Count > 0)
                {
                    var request = editorLoadRequests.Dequeue();
                    request.FinishLoad();
                }
            });
        }

        public void Load(ILoadResource loader, IResourceLoadedHandler loadHandler)
        {
            EditorLoadRequestPool.Dequeue().Initialize(loader, loadHandler).FinishLoad();
        }

        public void LoadAsync(ILoadResource loader, IResourceLoadedHandler loadHandler)
        {
            editorLoadRequests.Enqueue(EditorLoadRequestPool.Dequeue().Initialize(loader, loadHandler));
        }

        public void UnloadAll()
        {
            editorLoadRequests.Clear();
        }

        public void Dispose()
        {
            everyFrameUpdate.Cancel();
            UnloadAll();
        }

        private class EditorLoadRequest
        {
            private ILoadResource loadResource;
            private IResourceLoadedHandler resourceLoadedHandler;

            public EditorLoadRequest Initialize(ILoadResource loader, IResourceLoadedHandler loadedHandler)
            {
                loadResource = loader;
                resourceLoadedHandler = loadedHandler;

                return this;
            }

            public void FinishLoad()
            {
                try
                {
                    var editorPaths = UnityEditor.AssetDatabase.GetAssetPathsFromAssetBundleAndAssetName(loadResource.BundleName, loadResource.AssetName);

                    if (editorPaths.Length > 0)
                    {
                        resourceLoadedHandler.OnLoadComplete(UnityEditor.AssetDatabase.LoadAssetAtPath(editorPaths[0], typeof(Object)));
                    }
                    else
                    {
                        Debug.LogError($"Unable to find editor asset '{loadResource.AssetName}' in bundle '{loadResource.BundleName}'");
                    }
                }
                catch(Exception e)
                {
                    Debug.LogException(e);
                }
                finally
                {
                    EditorLoadRequestPool.Enqueue(this);
                }
            }
        }
    }
}
#endif