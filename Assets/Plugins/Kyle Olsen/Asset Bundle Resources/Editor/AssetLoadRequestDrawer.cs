﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace AssetBundleResources
{
    [CustomPropertyDrawer(typeof(AssetLoadRequest))]
    public class AssetLoadRequestDrawer : PropertyDrawer
    {
        private const string None = "None";
        private List<string[]> assetNames = new List<string[]>();
        private string[] bundleNames;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            try
            {
                SerializedProperty bundleName, assetName, bundleNameIndex, assetNameIndex;
                Initialize(property, out bundleName, out assetName, out bundleNameIndex, out assetNameIndex);

                var height = 0f;
                if (property.isExpanded)
                {
                    height += EditorGUI.GetPropertyHeight(bundleName);
                    height += EditorGUI.GetPropertyHeight(assetName);
                }

                return base.GetPropertyHeight(property, label) + height;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            return 0f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty bundleName, assetName, bundleNameIndex, assetNameIndex;

            try
            {
                Initialize(property, out bundleName, out assetName, out bundleNameIndex, out assetNameIndex);
                UpdateInts(bundleName, assetName, bundleNameIndex, assetNameIndex);

                EditorGUI.BeginProperty(position, label, property);
                EditorGUI.BeginChangeCheck();

                var titleRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
                property.isExpanded = EditorGUI.Foldout(titleRect, property.isExpanded, label, true);

                if (property.isExpanded)
                {
                    ++EditorGUI.indentLevel;

                    var bundleRect = new Rect(position.x, titleRect.y + titleRect.height, position.width, EditorGUI.GetPropertyHeight(bundleName));
                    var assetRect = new Rect(position.x, bundleRect.y + bundleRect.height, position.width, EditorGUI.GetPropertyHeight(assetName));
                    bundleNameIndex.intValue = EditorGUI.Popup(bundleRect, "Bundle Name", bundleNameIndex.intValue, bundleNames);
                    assetNameIndex.intValue = EditorGUI.Popup(assetRect, "Asset Name", assetNameIndex.intValue, assetNames[bundleNameIndex.intValue]);

                    --EditorGUI.indentLevel;
                }

                if (EditorGUI.EndChangeCheck())
                {
                    UpdateNames(bundleName, assetName, bundleNameIndex, assetNameIndex);
                    property.serializedObject.ApplyModifiedProperties();
                    property.serializedObject.Update();
                }

                EditorGUI.EndProperty();
            }
            catch
            {
                Initialize(property, out bundleName, out assetName, out bundleNameIndex, out assetNameIndex, true);
                bundleName.stringValue = "";
                assetName.stringValue = "";
                bundleNameIndex.intValue = 0;
                assetNameIndex.intValue = 0;
            }
        }

        private void Initialize(SerializedProperty property, out SerializedProperty bundleName, out SerializedProperty assetName, out SerializedProperty bundleNameIndex, out SerializedProperty assetNameIndex, bool force = false)
        {
            try
            {
                bundleName = property.FindPropertyRelative("bundleName");
                assetName = property.FindPropertyRelative("assetName");
                bundleNameIndex = property.FindPropertyRelative("bundleNameIndex");
                assetNameIndex = property.FindPropertyRelative("assetNameIndex");

                if (force || bundleNames == null)
                {
                    var bundleNamesList = new List<string> {None};
                    bundleNamesList.AddRange(AssetDatabase.GetAllAssetBundleNames());
                    bundleNames = bundleNamesList.ToArray();
                    assetNames = new List<string[]>();
                    foreach (var name in bundleNames)
                    {
                        var assetPathsList = new List<string>();
                        assetPathsList.Add(None);
                        assetPathsList.AddRange(AssetDatabase.GetAssetPathsFromAssetBundle(name));
                        var assetPaths = assetPathsList.ToArray();

                        for (var i = 0; i < assetPaths.Length; ++i)
                        {
                            assetPaths[i] = Path.GetFileNameWithoutExtension(assetPaths[i]);
                        }

                        assetNames.Add(assetPaths);
                    }
                }

                return;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            bundleName = null;
            bundleNameIndex = null;
            assetName = null;
            assetNameIndex = null;
        }

        private void UpdateInts(SerializedProperty bundleName, SerializedProperty assetName, SerializedProperty bundleNameIndex, SerializedProperty assetNameIndex)
        {
            try
            {
                for (var i = 0; i < bundleNames.Length; ++i)
                {
                    if (bundleName.stringValue == bundleNames[i])
                    {
                        bundleNameIndex.intValue = i;
                        break;
                    }
                }

                if (assetNames.Count > 0)
                {
                    for (var i = 0; i < assetNames[bundleNameIndex.intValue].Length; ++i)
                    {
                        if (assetName.stringValue == assetNames[bundleNameIndex.intValue][i])
                        {
                            assetNameIndex.intValue = i;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void UpdateNames(SerializedProperty bundleName, SerializedProperty assetName, SerializedProperty bundleNameIndex, SerializedProperty assetNameIndex)
        {
            try
            {
                var bundleValue = bundleNames[bundleNameIndex.intValue] != None ? bundleNames[bundleNameIndex.intValue] : "";
                bundleName.stringValue = bundleValue;
            }
            catch
            {
                bundleNameIndex.intValue = 0;
            }

            try
            {
                var assetValue = assetNames[bundleNameIndex.intValue][assetNameIndex.intValue] != None ? assetNames[bundleNameIndex.intValue][assetNameIndex.intValue] : "";
                assetName.stringValue = assetValue;
            }
            catch
            {
                assetNameIndex.intValue = 0;
            }
        }
    }
}