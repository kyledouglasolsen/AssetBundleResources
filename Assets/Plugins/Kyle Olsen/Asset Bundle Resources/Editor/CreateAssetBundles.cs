﻿using System;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AssetBundleResources
{
    public class CreateAssetBundles
    {
        public static bool IsCreating { get; private set; }

        public static void BuildAllAssetBundles(BuildAssetBundleOptions options = BuildAssetBundleOptions.UncompressedAssetBundle)
        {
            BuildAllAssetBundles(EditorUserBuildSettings.activeBuildTarget, options);
        }

        public static void BuildAllAssetBundles(BuildTarget buildTarget, BuildAssetBundleOptions options = BuildAssetBundleOptions.UncompressedAssetBundle)
        {
            IsCreating = true;
            var fullPath = AssetBundlePaths.FullPath;

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            new WaitForBuild(() =>
            {
                if (SceneManager.GetActiveScene().isDirty)
                {
                    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                }

                AssetDatabase.SaveAssets();

                BuildPipeline.BuildAssetBundles(FileUtil.GetProjectRelativePath(fullPath), options, buildTarget);
                Caching.ClearCache();
                Resources.UnloadUnusedAssets();
                AssetDatabase.Refresh();
                IsCreating = false;
            });
        }

        [MenuItem("Assets/Build AssetBundles (Force Rebuild)")]
        private static void ForceRebuildAllAssetBundles()
        {
            var fullPath = AssetBundlePaths.FullPath;
            if (Directory.Exists(fullPath))
            {
                Directory.Delete(fullPath, true);
            }

            BuildAllAssetBundles(BuildAssetBundleOptions.UncompressedAssetBundle | BuildAssetBundleOptions.ForceRebuildAssetBundle);
        }

        [MenuItem("Assets/Build AssetBundles")]
        private static void RebuildAllAssetBundles()
        {
            BuildAllAssetBundles();
        }

        [MenuItem("Assets/Clear Cache")]
        private static void ClearCache()
        {
            Caching.ClearCache();
        }

        [MenuItem("Assets/Print AssetBundles")]
        private static void GetNames()
        {
            var names = AssetDatabase.GetAllAssetBundleNames();
            foreach (var name in names)
            {
                Debug.Log("AssetBundle: " + name);
            }
        }

        public class WaitForBuild
        {
            private Action callback;

            public WaitForBuild(Action callback)
            {
                this.callback = callback;
                EditorApplication.update += Update;
                Update();
            }

            private void Update()
            {
                if (!BuildPipeline.isBuildingPlayer)
                {
                    callback();
                    EditorApplication.update -= Update;
                }
            }
        }
    }
}