﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace AssetBundleResources
{
    public class AssetBundleResourcesPostProcessBuild
    {
        private static bool disableNextGeneration;

        public static void DisableBundleGenerationNextBuild()
        {
            disableNextGeneration = true;
        }

        [PostProcessBuild(10000)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuildProject)
        {
            if (disableNextGeneration)
            {
                disableNextGeneration = false;
                return;
            }

            disableNextGeneration = false;

            var fileName = Path.GetFileNameWithoutExtension(pathToBuildProject);
            var buildDirectory = $"{Path.GetDirectoryName(pathToBuildProject)}/{fileName}_Data{AssetBundlePaths.RelativePath}";

            CreateAssetBundles.BuildAllAssetBundles(target);

            new CreateAssetBundles.WaitForBuild(() =>
            {
                var fullPath = AssetBundlePaths.FullPath;

                if (!Directory.Exists(fullPath))
                {
                    Debug.LogError("Unable to locate resources at path: " + fullPath);
                    return;
                }

                DirectoryCopy(fullPath, buildDirectory, true);
            });
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            var dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
            }

            var dirs = dir.GetDirectories();
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            if (copySubDirs)
            {
                foreach (var subdir in dirs)
                {
                    var temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, true);
                }
            }
        }
    }
}