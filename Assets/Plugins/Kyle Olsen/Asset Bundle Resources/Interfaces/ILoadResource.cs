﻿namespace AssetBundleResources
{
    public interface ILoadResource
    {
        string BundleName { get; }
        string AssetName { get; }
    }
}