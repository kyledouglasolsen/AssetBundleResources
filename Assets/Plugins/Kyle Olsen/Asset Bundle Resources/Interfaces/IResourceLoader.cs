﻿using System;

namespace AssetBundleResources
{
    public interface IResourceLoader : IDisposable
    {
        void Load(ILoadResource loader, IResourceLoadedHandler loadHandler);
        void LoadAsync(ILoadResource loader, IResourceLoadedHandler loadHandler);
        void UnloadAll();
    }
}