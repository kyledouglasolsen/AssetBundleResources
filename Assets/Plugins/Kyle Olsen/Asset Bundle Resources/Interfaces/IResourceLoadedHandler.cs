﻿using UnityEngine;

namespace AssetBundleResources
{
    public interface IResourceLoadedHandler
    {
        void OnLoadComplete(Object loadedObject);
    }
}