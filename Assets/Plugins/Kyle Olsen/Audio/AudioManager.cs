﻿using UnityEngine;
using System.Collections.Generic;

public class AudioManager : Singleton<AudioManager>
{
    private static int audioSourceCount, iterationIndex, maxRealVoices = -1;
    private static Transform audioSourceParent = null;
    private static PooledQueue<AudioSource> availableSources = new PooledQueue<AudioSource>(CreateSource);
    private static List<AudioSource> activeSources = new List<AudioSource>();

    public void Play(AudioEvent audioEvent, Vector3 position)
    {
        if (audioEvent == null)
        {
            return;
        }

        AudioSource source;
        do
        {
            source = availableSources.Dequeue();
        } while (source == null);
        
        source.transform.position = position;
        audioEvent.Play(source);
        activeSources.Add(source);
    }
    
    private void Update()
    {
        if (activeSources.Count == 0)
        {
            return;
        }

        if (iterationIndex < 0)
        {
            iterationIndex = activeSources.Count - 1;
            return;
        }

        var index = iterationIndex--;
        var source = activeSources[index];

        if (source.isPlaying)
        {
            return;
        }

        activeSources.RemoveAt(index);
        availableSources.Enqueue(source);
    }

    private static AudioSource CreateSource()
    {
        if (maxRealVoices == -1)
        {
            maxRealVoices = AudioSettings.GetConfiguration().numRealVoices;
        }

        if (audioSourceParent == null)
        {
            var parentGo = new GameObject("Audio Sources");
            DontDestroyOnLoad(parentGo);
            audioSourceParent = parentGo.transform;
        }

        if (audioSourceCount >= maxRealVoices)
        {
            var source = activeSources[0];
            activeSources.RemoveAt(0);
            return source;
        }

        ++audioSourceCount;
        var go = new GameObject($"Audio Source {audioSourceCount}");
        DontDestroyOnLoad(go);
        go.transform.SetParent(audioSourceParent);
        var newSource = go.AddComponent<AudioSource>();
        return newSource;
    }

    protected override void OnAwake()
    {
    }
}