﻿#if UNITY_EDITOR
using System.Linq;
#endif
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Audio Events/Simple")]
public class SimpleAudioEvent : AudioEvent
{
    [SerializeField] private AudioClip[] clips = new AudioClip[0];
    [SerializeField] private AudioMixerGroup mixer = null;
    [SerializeField] private RangedFloat volume = new RangedFloat(1f, 1f);
    [MinMaxRange(0f, 2f)] [SerializeField] private RangedFloat pitch = new RangedFloat(1f, 1f);
    [MinMaxRange(0f, 1f)] [SerializeField] private RangedFloat spatialBlend = new RangedFloat(1f, 1f);
    [MinMaxRange(0f, 1000f)] [SerializeField] private float minDistance = 1f, maxDistance = 500f;

    public override void Play(AudioSource source)
    {
        if (clips.Length == 0)
        {
            return;
        }

        var clip = clips[Random.Range(0, clips.Length)];

        if (clip == null)
        {
            return;
        }

        source.clip = clip;
        source.volume = Random.Range(volume.minValue, volume.maxValue);
        source.pitch = Random.Range(pitch.minValue, pitch.maxValue);
        source.spatialBlend = Random.Range(spatialBlend.minValue, spatialBlend.maxValue);
        source.minDistance = minDistance;
        source.maxDistance = maxDistance;
        source.outputAudioMixerGroup = mixer;
        source.Play();
    }

#if UNITY_EDITOR
    private void Reset()
    {
        clips = UnityEditor.Selection.objects.Where(x => (x as AudioClip) != null).Cast<AudioClip>().OrderBy(x => x.name).ToArray();
    }
#endif
}