﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEvent : MonoBehaviour
{
    [SerializeField] private AudioEvent audioEvent = null;

    private void OnEnable()
    {
        AudioManager.Ins.Play(audioEvent, transform.position);
    }
}
